# vsock: VM sockets using AF_VSOCK

## Purpose of this repository

The purpose of this repository is to collect useful information about vsock,
next steps, new ideas, and bugs. The
[Issues](https://gitlab.com/vsock/vsock/-/issues) page is used to keep
track of these.

## Community

- Matrix chat: https://matrix.to/#/#vsock:matrix.org
- Mailing list: virtualization@lists.linux-foundation.org
  - Archive: https://lore.kernel.org/virtualization/

## AF_VSOCK overview

Using `AF_VSOCK` you can easily allow applications in virtual machines and host
to communicate with the POSIX Sockets API. The existing applications that use
TCP/IP require few changes to be adapted. vsock is useful for lightweight VMs
since TCP/IP stack and network interfaces are not involved.

Compared to a Network Card Interface, where IP addresses for both guest and
host must be setup, the only thing to configure with vsock, is the CID
(Context ID) assigned to the VM. This can be configured by the user,
or automatically by the management tool.
In the guest, no configuration is needed and the host can be always reached
through the well defined CID (`VMADDR_CID_HOST`).

### Hypervisors supported

Several VMM and hypervisors supports AF_VSOCK:

- Cloud Hypervisor
- Firecracker
  - [hybrid vsock (vsock over unix domain socket)](https://github.com/firecracker-microvm/firecracker/blob/main/docs/vsock.md)
- Hyper-V
- QEMU/KVM
  - vhost-vsock
  - vhost-user-vsock (hybrid vsock - device emulated in an external process)
- VMware

#### virtio-vsock

virtio-vsock is a device supported by Linux and QEMU that provides AF_VSOCK
address family. It requires a minimal configuration: the setup phase does not
affect the guest running in the virtual machine at all, while in the host is
only required to assign an ID to each guest.

virtio-vsock spec: https://docs.oasis-open.org/virtio/virtio/v1.1/cs01/virtio-v1.1-cs01.html#x1-39000010

### OSs supported

Currently, Linux kernel is the only OS that supports AF_VSOCK.

AF_VSOCK core supports multiple transports depending on the hypervisor:

- hyperv_transport
  vsock over Hyper-V VMBus
- virtio_transport / vhost_transport
  virtio transports used by KVM hypervisors (e.g. QEMU, Firecracker, Cloud Hypervisor)
- vmci_transport
  VMCI transport for VMware hypervisor
- vsock_loopback
  Loopback transport for local communication (test/debug)

### Tools

Tools with AF_VSOCK support:

- wireshark >= 2.40 [2017-07-19]
- iproute2 >= 4.15 [2018-01-28]
  - ss
- tcpdump
  - merged in master [2019-04-16]
- nmap >= 7.80 [2019-08-10]
  - ncat
- nbd
  - nbdkit >= 1.15.5 [2019-10-19]
  - libnbd >= 1.1.6 [2019-10-19]
- iperf-vsock
  - iperf3 fork: https://github.com/stefano-garzarella/iperf-vsock
- socat >= 1.7.4 [2021-01-04]
- uperf
  - merged in master [2021-10-07]

### Languages

Languages providing AF_VSOCK bindings:

- C
  - glibc >= 2.18 [2013-08-10]
- Python
  - python >= 3.7 [2017-09-19]
- Golang
  - https://github.com/mdlayher/vsock
- Rust
  - libc crate >= 0.2.59 [2019-07-08]
    - struct sockaddr_vm
    - VMADDR_- macros
  - nix crate >= 0.15.0 [2019-08-10]
    - VSOCK supported in the socket API (nix::sys::socket)

## Links

### Presentations

- [Leveraging virtio-vsock in the cloud and containers](https://archive.fosdem.org/2021/schedule/event/vai_virtio_vsock/) @ FOSDEM 2021
- [VSOCK: VM↔host socket with minimal configuration](https://devconfcz2020a.sched.com/event/YOwb/vsock-vm-host-socket-with-minimal-configuration) @ DevConf.CZ 2020
- [virtio-vsock in QEMU, Firecracker and Linux: Status, Performance and Challenges](https://sched.co/TmwK) @ KVM Forum 2019
- [virtio-vsock: Zero-configuration host/guest communication](https://kvmforum2015.sched.com/event/3Wch/virtio-vsock-zero-configuration-hostguest-communication-stefan-hajnoczi) @ KVM Forum 2015
